CC := gcc
CFLAGS := -std=c99 -Wall -Wextra -pedantic
LDFLAGS := -ledit -lm
BIN = byolisp
SRC = byolisp.c mpc.c

all: install

install: $(SRC)
	$(CC) $(CFLAGS) $(SRC) -o $(BIN) $(LDFLAGS)

debug: $(SRC)
	$(CC) $(CFLAGS) -g $(SRC) -o $(BIN) $(LDFLAGS)

clean:
	$(RM) -r *.dSYM $(BIN)

.PHONY: all install clean
