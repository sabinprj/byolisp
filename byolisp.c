#include "mpc.h" /* Rest of the headers are included via mpc.h */

#define LASSERT(args, cond, err) \
    if (!(cond)) { lval_del(args); return lval_err(err); }

/* If on Windows, use these functions */
#ifdef _WIN32
#define BUFLEN 2048

static char buffer[BUFLEN];

/* Fake readline function */
char *readline(char *prompt) {
    fputs(prompt, stdout);
    fgets(buffer, BUFLEN, stdin);

    char *cpy = malloc(strlen(buffer) + 1);

    strcpy(cpy, buffer);

    cpy[strlen(cpy) - 1] = '\0';

    return cpy;
}

/* Fake add_history function */
void add_history(char *unused) {}

#else
/* For platforms that are not Windows */
#include <editline/readline.h>
#endif

/* Declare new lval struct */
typedef struct lval {
    /* Error and symbol have some string data */
    char *err;
    char *sym;
    int type;
    int count;
    double num;
    /* Count and pointer to list of "lval *" */
    struct lval** cell;
} lval;

lval *lval_num(double x);
lval *lval_err(char *m);
lval *lval_sym(char *s);
lval *lval_sexpr(void);
lval *lval_qexpr(void);
lval *lval_read_num(mpc_ast_t *t);
lval *lval_read(mpc_ast_t *t);
lval *lval_add(lval *v, lval *x);
lval *lval_eval_sexpr(lval *v);
lval *lval_eval(lval *v);
lval *lval_pop(lval *v, int i);
lval *lval_take(lval *v, int i);
lval *builtin_op(lval *a, char *op);
lval *builtin_head(lval*a);
lval *builtin_tail(lval *a);
lval *builtin_list(lval *a);
lval *builtin_eval(lval *a);
lval *builtin_join(lval *a);
lval *builtin_init(lval *a);
lval *builtin_cons(lval *a);
lval *builtin_len(lval *a);
lval *builtin(lval *a, char *func);
lval *lval_join(lval *x, lval *y);
void lval_del(lval *v);
void lval_expr_print(lval *v, char open, char close);
void lval_print(lval *v);
void lval_println(lval *v);

/* Create Enumeration of possible lval types */
enum { LVAL_NUM, LVAL_ERR, LVAL_SYM, LVAL_SEXPR, LVAL_QEXPR };

/* Create enumeration of possible error types */
enum { LERR_DIV_ZERO, LERR_BAD_OP, LERR_BAD_NUM };

/* construct a pointer to new Number lval */
lval *lval_num(double x)
{
    lval *v = malloc(sizeof(lval));
    v->type = LVAL_NUM;
    v->num = x;
    return v;
}

/* construct a pointer to new Error lval */
lval *lval_err(char *m)
{
    lval *v = malloc(sizeof(lval));
    v->type = LVAL_ERR;
    size_t size = strlen(m) + 1;
    v->err = malloc(size);
    memcpy(v->err, m, size);

    return v;
}

/* construct a pointer to a new Symbol lval */
lval *lval_sym(char *s)
{
    lval *v = malloc(sizeof(lval));
    v->type = LVAL_SYM;
    size_t size = strlen(s) + 1;
    v->sym = malloc(size);
    memcpy(v->sym, s, size);

    return v;
}

/* a pointer to a new empty Sexpr lval */
lval *lval_sexpr(void)
{
    lval *v = malloc(sizeof(lval));
    v->type = LVAL_SEXPR;
    v->count = 0;
    v->cell = NULL;

    return v;
}

/* a pointer to a new empty Qexpr lval */
lval *lval_qexpr(void)
{
    lval *v = malloc(sizeof(lval));
    v->type = LVAL_QEXPR;
    v->count = 0;
    v->cell = NULL;

    return v;
}

/* Cleanup memory after */
void lval_del(lval *v)
{
    if (v != NULL) {
        switch (v->type) {
            /* No cleanup required for number type */
            case LVAL_NUM:
                break;

            /* For Err or Sym free the string data */
            case LVAL_ERR:
                if (v->err != NULL) {
                    free(v->err);
                    v->err = NULL;
                }

                break;
            case LVAL_SYM:
                if (v->sym != NULL) {
                    free(v->sym);
                    v->sym = NULL;
                }

                break;
            /* If it is Sexpr or Qexpr, then delete all elements inside */
            case LVAL_QEXPR:
            case LVAL_SEXPR:
                for (int i = 0; i < v->count; i++) {
                    lval_del(v->cell[i]);
                }

                /* Also free the memory allocate to contain the pointers */
                if (v->cell != NULL) {
                    free(v->cell);
                    v->cell = NULL;
                }

                break;
        }

        /* Free the memory allocated for the "lval" struct itself */
        free(v);
        v = NULL;
    }
}

lval *lval_read_num(mpc_ast_t *t)
{
    errno = 0;
    double x = atof(t->contents);

    return errno != ERANGE ? lval_num(x) : lval_err("invalid number");
}

lval *lval_read(mpc_ast_t *t)
{
    /* If Symbol or Number return conversion to that type */
    if (strstr(t->tag, "number")) { return lval_read_num(t); }
    if (strstr(t->tag, "symbol")) { return lval_sym(t->contents); }

    /* If root (>) or Sexpr then create empty list */
    lval *x = NULL;

    if (strcmp(t->tag, ">") == 0) {
        x = lval_sexpr();
    } else if (strstr(t->tag, "sexpr")) {
        x = lval_sexpr();
    } else if (strstr(t->tag, "qexpr")) {
        x = lval_qexpr();
    }

    /* Fill this list with any valid expression contained within */

    for (int i = 0; i < t->children_num; i++) {
        lval *ret = NULL;
        if (strcmp(t->children[i]->contents, "(") == 0) { continue; }
        if (strcmp(t->children[i]->contents, ")") == 0) { continue; }
        if (strcmp(t->children[i]->contents, "{") == 0) { continue; }
        if (strcmp(t->children[i]->contents, "}") == 0) { continue; }
        if (strcmp(t->children[i]->tag,  "regex") == 0) { continue; }

        ret = lval_read(t->children[i]);
        x = lval_add(x, ret);
    }
    return x;
}

lval *lval_add(lval *v, lval *x)
{
    if (v != NULL) {
        v->count++;
        v->cell = realloc(v->cell, sizeof(lval *) * (unsigned long) v->count);
        v->cell[v->count-1] = x;
    }
    return v;
}

lval *lval_eval_sexpr(lval *v)
{
    /* Evaluate children */
    for (int i = 0; i < v->count; i++) {
        v->cell[i] = lval_eval(v->cell[i]);
    }

    /* Error checking */
    for (int i = 0; i < v->count; i++) {
        if (v != NULL && v->cell[i] != NULL && v->cell[i]->type == LVAL_ERR) {
            return lval_take(v, i);
        }
    }

    /* Empty expressions */
    if (v->count == 0) { return v; }

    /* Single expressions */
    if (v->count == 1) { return lval_take(v, 0); }

    /* Ensure first element is a symbol */
    lval *f = lval_pop(v, 0);

    if (f->type != LVAL_SYM) {
        lval_del(f);
        lval_del(v);

        return lval_err("S-expression does not start with symbol!");
    }

    /* Call builtin with operator */
    lval *result = builtin(v, f->sym);
    lval_del(f);

    return result;
}

lval *lval_eval(lval *v)
{
    /* Evaluate Sexpressions */
    if (v != NULL && v->type == LVAL_SEXPR) { return lval_eval_sexpr(v); }

    /* All other lval types remain the same */
    return v;
}

lval *lval_pop(lval *v, int i)
{
    /* Find the item at "i" */
    lval *x = v->cell[i];

    /* Shift memory after the item at "i" over the top */
    memmove(&v->cell[i], &v->cell[i+1], sizeof(lval *) * (v->count-i-1));

    /* Decrease the count of the items in the list */
    v->count--;

    /* Reallocate the memory used */
    v->cell = realloc(v->cell, sizeof(lval *) * v->count);

    return x;
}

lval *lval_take(lval *v, int i)
{
    lval *x = lval_pop(v, i);
    lval_del(v);

    return x;
}

lval *builtin_op(lval *a, char *op)
{
    /* Ensure all arguments are numbers */
    for (int i = 0; i < a->count; i++) {
        if (a->cell[i]->type != LVAL_NUM) {
            lval_del(a);
            return lval_err("Cannot operate on non-number!");
        }
    }

    /* Pop the first element */
    lval *x = lval_pop(a, 0);

    /* If no arguments  and sub then perform unary negation */
    if ((strcmp(op, "-") == 0) && a->count == 0) {
        x->num = -x->num;
    }

    /* While there are still elements remaining */
    while (a->count > 0) {

        /* Pop the next element */
        lval *y = lval_pop(a, 0);

        if (strcmp(op, "+") == 0) { x->num += y->num; }
        if (strcmp(op, "-") == 0) { x->num -= y->num; }
        if (strcmp(op, "*") == 0) { x->num *= y->num; }
        if (strcmp(op, "/") == 0) {
            if (y->num == 0) {
                lval_del(x);
                lval_del(y);
                x = lval_err("Division by Zero!");
                break;
            }
            x->num /= y->num;
        }

        if (strcmp(op, "%") == 0) {
            if (y->num == 0) {
                lval_del(x);
                lval_del(y);
                x = lval_err("Modular Division by Zero!");
                break;
            }
            x->num = fmod(x->num, y->num);
        }

        if (strcmp(op, "^") == 0) { x->num = pow(x->num, y->num); }

        lval_del(y);
    }

    lval_del(a);
    return x;
}

lval *builtin_head(lval *a)
{
    /* second argument is negated in macro */
    LASSERT(a, a->count == 1,
            "Function 'head' passed too many arguments!");

    LASSERT(a, a->cell[0]->type == LVAL_QEXPR,
            "Function 'head' passed incorrect types!");

    LASSERT(a, a->cell[0]->count != 0,
            "Function 'head' passed {}!");

    /* Otheriwse take first argument */
    lval *v = lval_take(a, 0);

    /* Delete all elements that are not head and return */
    while (v->count > 1) {
        lval_del(lval_pop(v, 1));
    }

    return v;
}

lval *builtin_tail(lval *a)
{
    /* Check error conditions */
    LASSERT(a, a->count == 1,
            "Function 'head' passed too many arguments!");

    LASSERT(a, a->cell[0]->type == LVAL_QEXPR,
            "Function 'head' passed incorrect types!");

    LASSERT(a, a->cell[0]->count != 0,
            "Function 'head' passed {}!");

    /* Take first argument */
    lval *v = lval_take(a, 0);

    /* Delete first element and return */
    lval_del(lval_pop(v, 0));

    return v;
}

lval *builtin_list(lval *a)
{
    a->type = LVAL_QEXPR;
    return a;
}

lval *builtin_eval(lval *a)
{
    LASSERT(a, a->count == 1,
            "Function 'eval' passed too many arguments!");
    LASSERT(a, a->cell[0]->type == LVAL_QEXPR,
            "Function 'eval' passed incorrect type!");

    lval *x = lval_take(a, 0);
    x->type = LVAL_SEXPR;

    return lval_eval(x);
}

lval *builtin_join(lval *a)
{
    for (int i = 0; i < a->count; i++) {
        LASSERT(a, a->cell[i]->type == LVAL_QEXPR,
                "Function 'join' passed incorrect type.");
    }

    lval *x = lval_pop(a, 0);

    while (a->count) {
        x = lval_join(x, lval_pop(a, 0));
    }

    lval_del(a);

    return x;
}

lval *builtin_init(lval *a)
{
    LASSERT(a, a->count == 1,
            "Function 'init' passed too many arguments!");
    LASSERT(a, a->cell[0]->type == LVAL_QEXPR,
            "Function 'init' passed incorrect types!");
    LASSERT(a, a->cell[0]->count != 0,
            "Function 'init' passed {}!");

    lval *x = lval_pop(a, 0);

    lval_del(lval_pop(x, x->count-1));

    return x;
}

lval *builtin_cons(lval *a)
{
    LASSERT(a, a->count == 2,
            "Function 'cons' expects two arguments!");
    LASSERT(a, a->cell[0]->type == LVAL_NUM,
            "Function 'cons' first argument should be number!");
    LASSERT(a, a->cell[1]->type == LVAL_QEXPR,
            "Function 'cons' second argument should be Q-expression!");

    lval *q = lval_qexpr();
    lval *x = lval_add(q, lval_pop(a, 0));

    while (a->count) {
        x = lval_join(x, lval_pop(a, 0));
    }

    lval_del(a);

    return x;
}

lval *builtin_len(lval *a)
{
    LASSERT(a, a->count == 1,
            "Function 'len' passed too many arguments!");
    LASSERT(a, a->cell[0]->type == LVAL_QEXPR,
            "Function 'len' passed incorrect type(s)!");

    lval *x = lval_pop(a, 0);

    lval *len = lval_num(x->count);

    lval_del(x);
    lval_del(a);

    return len;
}

lval *builtin(lval *a, char *func)
{
    if (strcmp("list", func) == 0) { return builtin_list(a); }
    if (strcmp("head", func) == 0) { return builtin_head(a); }
    if (strcmp("tail", func) == 0) { return builtin_tail(a); }
    if (strcmp("join", func) == 0) { return builtin_join(a); }
    if (strcmp("eval", func) == 0) { return builtin_eval(a); }
    if (strcmp("init", func) == 0) { return builtin_init(a); }
    if (strcmp("cons", func) == 0) { return builtin_cons(a); }
    if (strcmp("len", func) == 0) { return builtin_len(a); }
    if (strstr("+-/*%^", func)) { return builtin_op(a, func); }
    lval_del(a);

    return lval_err("Unknown function!");
}

lval *lval_join(lval *x, lval *y)
{
    /* For each cell in 'y', add it to 'x' */
    while (y->count) {
        x = lval_add(x, lval_pop(y, 0));
    }

    /* Delete the empty 'y', and return 'x' */
    lval_del(y);

    return x;
}

void lval_expr_print(lval *v, char open, char close)
{
    putchar(open);

    for (int i = 0; i < v->count; i++) {
        /* Print value contained within */
        lval_print(v->cell[i]);

        if (i != (v->count-1)) {
            putchar(' ');
        }
    }

    putchar(close);
}

void lval_print(lval *v)
{
    if (v != NULL) {
        switch(v->type) {
            case LVAL_NUM:
                printf("%g", v->num);
                break;
            case LVAL_ERR:
                printf("Error: %s", v->err);
                break;
            case LVAL_SYM:
                printf("%s", v->sym);
                break;
            case LVAL_SEXPR:
                lval_expr_print(v, '(', ')');
                break;
            case LVAL_QEXPR:
                lval_expr_print(v, '{', '}');
                break;
        }
    }
}

void lval_println(lval *v)
{
    lval_print(v);
    putchar('\n');
}

int main(void)
{
    /* Create Some Parsers */
    mpc_parser_t *Number = mpc_new("number");
    mpc_parser_t *Symbol = mpc_new("symbol");
    mpc_parser_t *Sexpr  = mpc_new("sexpr");
    mpc_parser_t *Qexpr  = mpc_new("qexpr");
    mpc_parser_t *Expr   = mpc_new("expr");
    mpc_parser_t *Lispy  = mpc_new("lispy");

    mpca_lang(MPCA_LANG_DEFAULT,
              "number   : /-?[0-9]+(\\.[0-9]+)?/;"
              "symbol   : '+' | '-' | '*' | '/' | '%' | '^' | "
                          "\"list\" | \"head\" | \"tail\" | \"join\" | "
                          "\"init\" | \"cons\" | \"len\";"
              "sexpr    : '(' <expr>* ')';"
              "qexpr    : '{' <expr>* '}';"
              "expr     : <number> | <symbol> | <sexpr> | <qexpr> ;"
              "lispy    : /^/ <expr>* /$/;",
              Number, Symbol, Sexpr, Qexpr, Expr, Lispy);

    /* Print Version and Exit Information */
    puts("byolisp version 0.0.1");
    puts("Press Ctrl+c or Ctrl+d to exit\n");

    while(1) {
        char* input = readline("byolisp> ");
        if (input == NULL)
            break;

        add_history(input);

        /* Attempt to parse the user input */
        mpc_result_t r;

        if (mpc_parse("<stdin>", input, Lispy, &r)) {
            lval *x = lval_eval(lval_read(r.output));
            lval_println(x);
            lval_del(x);
            /* mpc_ast_delete(r.output); */
        } else {
            /* Print Error */
            mpc_err_print(r.error);
            mpc_err_delete(r.error);
        }

        free(input);
    }

    /* Undefine and delete our parser */
    mpc_cleanup(6, Number, Symbol, Sexpr, Qexpr, Expr, Lispy);


    return 0;
}
